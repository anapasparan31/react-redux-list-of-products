export interface ICategory {
  id: string;
  title: string;
  box_limit: number;
  is_default: boolean;
  recently_added: boolean;
  hidden: boolean;
  pivot?: any;
}

export interface IProduct {
  id: string;
  title: string;
  description: string;
  image: string;
  isForSale: boolean;
  price: string;
  categories: ICategory[];
}

export interface IAppState {
  categories: ICategory[];
  products: IProduct[];
}

export interface IRootState {
  state: IAppState;
}

export interface IAppActions {
  fetchCategories: () => Promise<ICategory[]>;
  fetchProducts: () => Promise<IProduct[]>;
}

export type TStore = IRootState & IAppActions;
