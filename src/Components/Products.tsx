import * as React from 'react';
import { Collapse, Popover } from 'antd';

import { IProduct } from '../Interfaces';


interface IProps {
  products: IProduct[];
}

export default class Products extends React.Component<IProps, {}> {
    render() {
        console.log('render Products');

        const { products } = this.props;

        return (
            <Collapse
                bordered={true}
            >
            { !!products && products.map((item: IProduct) => (
              
                <Collapse.Panel header={item.title} key={item.id}>
                    <Popover content={(
                        <div>
                            <img alt={item.id} src={item.image} />
                            <div>Price: £{item.price}</div>
                        </div>
                    )} title={item.title}>
                    <div>{item.description}</div>
                    </Popover>

                </Collapse.Panel>
            ))}
            </Collapse>
        );
    }
}
