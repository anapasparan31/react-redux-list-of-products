import * as React from 'react';
import { Menu } from 'antd';
import { ICategory } from '../Interfaces/index';

const MenuItem = Menu.Item;

interface IProps {
    categories: ICategory[];
    showProductsByCategory: any;
}

interface IState {
    hasError: boolean;
}
export default class Header extends React.Component<IProps, IState> {
    // Catching JavaScript errors occurred within the component
    componentDidCatch() {
        this.setState({ ...this.state, hasError: true });
    }
    showProductsByCategory(category: any) {
       this.props.showProductsByCategory(category.key);
    }

    render (){
        console.log('render Header');

        return (
            <Menu mode='horizontal' onClick={this.showProductsByCategory.bind(this)}>
                <MenuItem key='allCategories'>All</MenuItem>
                {(this.props.categories.length) && this.props.categories.map((category: ICategory) => (<MenuItem key={category.id}>{category.title}
                </MenuItem>))}
            </Menu>
        );
    }
}
