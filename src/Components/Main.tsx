import * as React from 'react';
import { Input, Layout } from 'antd';

import {TStore, IProduct, ICategory} from '../Interfaces';
import Header from './Header';
import Products from './Products';

const Search = Input.Search;

interface IState {
  hasError: boolean;
  productsToShow: IProduct[];
}

export default class Main extends React.PureComponent<TStore, IState> {

    constructor( state: TStore ) {
        super(state);
        this.state = {
            productsToShow: this.props.state.products,
            hasError: false
        };
        this.showProductsByCategory = this.showProductsByCategory.bind(this);
        this.filterProducts = this.filterProducts.bind(this);
        this.cleanSearchResults = this.cleanSearchResults.bind(this);
    }

    componentWillReceiveProps(nextProps: any) {
        if (nextProps.state.products.length > 0 && this.props.state.products.length === 0) {
            this.setState({ ...this.state, productsToShow: nextProps.state.products});
        }
    }

    // Catching JavaScript errors occurred within the component
    componentDidCatch() {
        this.setState({ ...this.state, hasError: true });
    }

    showProductsByCategory(categoryId: string) {
        if (categoryId === 'allCategories') {
            this.setState({ ...this.state, productsToShow: this.props.state.products});

        } else {
            const productsByCategory = this.props.state.products.filter(( prod: IProduct) =>
                prod.categories.find(category => (category.id === categoryId )));
            this.setState({ ...this.state, productsToShow: productsByCategory});
        }

    }

    filterProducts(value: string) {
       const filteredProducts = this.props.state.products.filter(( prod: IProduct) =>
           (prod.title.search(value) > -1 || prod.description.search(value) > -1));
       this.setState({ ...this.state, productsToShow: filteredProducts});
    }
    
    cleanSearchResults(e: any) {
        const value = e.target.value;
        if(!value) {
            const filteredProducts = this.props.state.products.filter(( prod: IProduct) =>
                (prod.title.search(value) > -1 || prod.description.search(value) > -1));
            this.setState({ ...this.state, productsToShow: filteredProducts});
        }
    }

    filterCategoriesWithProducts(): ICategory[] {
        const { categories, products } = this.props.state;
        const filteredCategories: ICategory[] = [];
        if(categories && products) {            
            categories.forEach(category => {                
                const productFound = products.find(product => 
                        product.categories.some(cat => cat.id === category.id)
                    )
                return productFound ? filteredCategories.push(category) : null
            });
        }
        return filteredCategories;
    }

    render() {
        console.log('render Main');
        
        const { productsToShow } = this.state
        const products = productsToShow;
        const filteredCategories: ICategory[] = this.filterCategoriesWithProducts();

        return (
        <Layout className='layout container'>
            <div className='fixContent'>
                <Header categories={filteredCategories}  showProductsByCategory={this.showProductsByCategory}/>
                <Search
                    className='searchInput'
                    placeholder='Search product'
                    onChange={this.cleanSearchResults}
                    onSearch={this.filterProducts}
                    enterButton="Search"
                    allowClear
                />
            </div>
            <Layout.Content className='scrollableContent'>
                <Products products={products}/>
            </Layout.Content>
        </Layout>
        );
    }
}

