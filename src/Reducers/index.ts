import { handleActions } from 'redux-actions';
import { FETCH_CATEGORIES, FETCH_PRODUCTS} from '../Constants';

export const appReducer = handleActions({
    [FETCH_CATEGORIES]: (state, action) => {
        return Object.assign({}, state, {
            categories: action.payload
        });
    },

    [FETCH_PRODUCTS]: (state, action) => {
        return Object.assign({}, state, {
            products: action.payload
        });
    },
},
{
    categories: [],
    products: []
});
