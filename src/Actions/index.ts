import { createAction } from 'redux-actions';
import { FETCH_CATEGORIES, FETCH_PRODUCTS, FILTER_CATEGORIES } from '../Constants';
import { IProduct, ICategory } from '../Interfaces';

const baseURL = 'https://api.gousto.co.uk/products/v2.0';
const categoriesUrl = `${baseURL}/categories`;
const productsUrl = `${baseURL}/products?includes[]=categories&includes[]=attributes&image_sizes[]=400`;

export const fetchCategories = createAction( FETCH_CATEGORIES, async () => {
    const { data } = await fetch( categoriesUrl ).then( (rsp:any) => rsp.json() );
    const categories = data.filter((item: any) => !item.hidden)
    return categories;
});

export const fetchProducts = createAction( FETCH_PRODUCTS, async () => {
    const { data } = await fetch( productsUrl ).then( (rsp:any) => rsp.json() );
    return data.map((item: any) => {
        return {
            id: item.id,
            title: item.title,
            description: item.description,
            image: item.images['400'] && item.images['400'].src,
            isForSale: item.is_for_sale,
            price: item.list_price,
            categories: item.categories,
        };
    });
});

