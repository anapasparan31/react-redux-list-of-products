const { join } = require( "path" ),
      pkg = require( "./package.json" ),
      CleanWebpackPlugin = require( "clean-webpack-plugin" ),
      SRC_FULL_PATH = join( __dirname, "./src/" ),
      PUBLIC_PATH = "./build/",
      PUBLIC_FULL_PATH = join( __dirname, PUBLIC_PATH );

module.exports = {
    // Application entry scripts
    entry: [
      'babel-polyfill',
      'whatwg-fetch',
      // script alias : path
      join( SRC_FULL_PATH, "index.tsx" )
    ],
    // Output configuration for Webpack
    output: {
			path: PUBLIC_FULL_PATH,
			filename: `index.js`,
      chunkFilename: `[name].v${pkg.version}.widget.js`,
      publicPath: PUBLIC_PATH
    },

    resolve: {
       modules: [
        "node_modules",
        SRC_FULL_PATH
      ],
      extensions: [ ".ts", ".tsx", ".js", ".css" ]
    },

    plugins: [
      // cleaning up the build directory prior to update
      new CleanWebpackPlugin([ PUBLIC_PATH ])
    ],

    devtool: "source-map", // enum
    module: {
			loaders: [
                {
                    test: /\.js$/,
                    loader: 'babel-loader',
                    exclude: /node_modules/,
                    query: {
                      plugins: ['transform-runtime'],
                      presets: ['es2015', 'stage-0', 'react'],
                    }
                },
                {
                    test: /\.tsx?$/,
                    loader: 'ts-loader',
                    exclude: /node_modules/
                }
            ]
		}
};
