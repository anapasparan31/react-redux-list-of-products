# Application developed with React + Redux + TypeScript

The bundle contains React/Redux application, made of TypeScript modules and ready-made components of [Ant Design](https://ant.design/).
The scope of the application is to list products and categories.

## Available Automation Commands
- `npm run build` - build app
- `npm run watch` - watch for updates in the code base
- `npm start` - start app
- `npm run lint` - ensure the code follows the code style conventions (see [tslint](./tslint))

## Steps for starting the app
- `npm install` - install all the dependeces
- `npm run build` - build the entry index file
- `npm run start` - starting the app

## package.json

- `ts-loader` - [TypeScript loader](https://github.com/TypeStrong/ts-loader) for Webpack
- `babel-polyfill` -[Babel polyfill](https://babeljs.io/docs/usage/polyfill/)
- `whatwg-fetch` -[Fetch polyfill](https://github.com/github/fetch) support feth on IE
- `tslint` - [TypeScript code linter](https://github.com/palantir/tslint)
- `typescript` - [TypeScript](https://www.typescriptlang.org/)
- `http-server` - [Command-line http server](https://github.com/indexzero/http-server)
- `webpack` - [Webpack bundler](https://github.com/webpack/webpack)
- `material-ui` - [Ant Design](https://ant.design/) for ready-made UI components
- `prop-types` - [React Type Checker](https://github.com/facebook/prop-types)
- `react` - [React Library](https://github.com/facebook/react)
- `react-redux` - [Utility](https://github.com/reactjs/react-redux) to connect React components to the Redux store
- `redux` - [Redux State Container](https://redux.js.org/)
- `redux-actions` - [Helper](https://github.com/reduxactions/redux-actions) to create actions and reducers
- `redux-promise` - [Redux Store Middleware](https://github.com/acdlite/redux-promise) to handle promisable actions
- `redux-thunk` - [Redux Store Middleware](https://github.com/gaearon/redux-thunk) to handle async actions
